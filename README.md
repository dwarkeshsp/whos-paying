# 2020 Guide

2020guide.me is meant to inform voters on the candidates running for the 2020 presidential race, their positions, and relevant polling.

Members:
    Alexander Houy
    Mitchell Kaysen
    Jeffrey Malone
    Dwarkesh Patel
    Jay Seaholm, jcs5764, JCSeaholm
    Darrick Wu
    
Git SHA: 
GitLab Pipelines: https://gitlab.com/dwarkeshsp/whos-paying/pipelines
Website: https://www.2020guide.me
Estimated Completion Time, per Member (Hours):
    Alexander Houy:
    Mitchell Kaysen:
    Jeffrey Malone:
    Dwarkesh Patel:
    Jay Seaholm: 
    Darrick Wu:

Actual Completion Time, per Member (Hours):
    Alexander Houy:
    Mitchell Kaysen:
    Jeffrey Malone:
    Dwarkesh Patel:
    Jay Seaholm: 
    Darrick Wu:

Comments:
