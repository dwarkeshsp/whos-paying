/*These are our tests of every endpoint in the API, so they are generic
and only work within the Postman environment.
These tests make sure that we receive a response from the endpoint we're testing,
and that the response is in JSON format.
The only endpoint exempt from these tests is the api.2020guide.me endpoint,
which produces our documentation in HTML format*/

// Checking that the response is happening in a reasonable amount of time
pm.test("Response time is less than 500ms", function () {
    pm.expect(pm.response.responseTime).to.be.below(500);
});

// Checking status code is 200 (ok)
pm.test("Status is 200 (ok)", function () {
    pm.response.to.have.status(200);
});

// Checking valid response format
pm.test("Response must be in JSON", function () {
     // pm.response.to.be.withBody; does the response have any info?
     pm.response.to.be.json; // this assertion also checks if a response exists, so it shouldn't just be empty (above check redundant)
});

/*Tests of our documentation page at api.2020guide.me:*/

// Checking that our API Documentation Page is being displayed properly
pm.test("Body is correct (matches our HTML file", function () {
    var APIDocs = ["Endpoints:", "/candidates", "/issues", "/polls"];
    for (var APIfeature of APIDocs) {
        pm.expect(pm.response.text()).to.include(APIfeature);
    }
});