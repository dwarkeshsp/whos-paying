/*jshint loopfunc:true */

import React, { Component } from "react";
import { Tab, Tabs } from "react-mdl";

const People = ({ person }) => {
  return (
    <div>
      <center>
        <img
          style={{ width: "20%", height: "20%" }}
          src={require("../about-data/" + person.picture)}
          alt=""
        />
      </center>

      <center>
        <div>
          <h5>{person.bio}</h5>
        </div>
      </center>
      <center>
        <div>
          <h6>Issues Closed: {person.closed_issues}</h6>
        </div>
      </center>
      <center>
        <div>
          <h6>Commits Made: {person.commits}</h6>
        </div>
      </center>
      <center>
        <div>
          <h6>Unit Tests Made: {person.unit_tests}</h6>
        </div>
      </center>
    </div>
  );
};

function loadData(key, data) {
  fetch(
    "https://gitlab.com/api/v4/projects/12977992/issues_statistics?author_id=" +
      data[key].id
  )
    .then(results => results.json())
    .then(
      my_data => (data[key]["closed_issues"] = my_data.statistics.counts.closed)
    );
}

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
      data: require("../about-data/People.json"),
      commits: 0,
      issues: 0
    };
  }

  async componentDidMount() {
    var keys = Object.keys(this.state.data);
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      loadData(key, this.state.data);
    }

    await fetch("https://gitlab.com/api/v4/projects/12977992/issues_statistics")
      .then(result => result.json())
      .then(data => {
        this.state.issues = data.statistics.counts.all;
      });

    var page = 1;
    var done = false;

    while (!done) {
      await fetch(
        "https://gitlab.com/api/v4/projects/12977992/repository/commits?all=true&page=" +
          page
      )
        // eslint-disable-next-line
        .then(result => {
          done = !result.ok;

          return result.json();
        }) // eslint-disable-next-line
        .then(list => {
          list.forEach(commit => {
            this.state.commits += 1;
            var name = commit.committer_name;
            var keys = Object.keys(this.state.data);
            keys.forEach(person => {
              if (name === this.state.data[person].gitlab_name) {
                this.state.data[person].commits += 1;
              }
            });
          });

          if (list.length < 20) {
            done = true;
          }
        });

      page += 1;
    }
    this.setState({ activeTab: 0 });
  }

  render() {
    return (
      <div className="tabs">
        <Tabs
          activeTab={this.state.activeTab}
          onChange={tabId => this.setState({ activeTab: tabId })}
          ripple
        >
          <Tab>{this.state.data.Alexander_Houy.name}</Tab>
          <Tab>{this.state.data.Mitchell_Kaysen.name}</Tab>
          <Tab>{this.state.data.Jeffrey_Malone.name}</Tab>
          <Tab>{this.state.data.Dwarkesh_Patel.name}</Tab>
          <Tab>{this.state.data.Jay_Seaholm.name}</Tab>
          <Tab>{this.state.data.Darrick_Wu.name}</Tab>
        </Tabs>
        <section>
          <div className="content">
            {" "}
            {
              <People
                person={
                  this.state.data[
                    Object.keys(this.state.data)[this.state.activeTab]
                  ]
                }
              />
            }
          </div>
        </section>

        <center>
          <h4>Total Commits: {this.state.commits}</h4>
        </center>
        <center>
          <h4>Total Issues: {this.state.issues}</h4>
        </center>
        <center>
          <h4>
            Our repository can be found{" "}
            <a href="https://gitlab.com/dwarkeshsp/whos-paying">here.</a>
          </h4>
          <h4>
            Our API can be found <a href="http://api.2020guide.me">here.</a>
          </h4>
          <h3>
              Tools Used:
          </h3>
          <h5>
              Amazon Web Services: Used to host our frontend and backend.
          </h5>
          <h5>
              Namecheap: Used to get a pretty url.
          </h5>
          <h5>
              React/Bootstrap: Used to create the UI.
          </h5>
          <h5>
              Flask, SQLAlchemy, Flask-Restless: Used to connect to and present database data.
          </h5>
        </center>
      </div>
    );
  }
}

export default About;
