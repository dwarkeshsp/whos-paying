/*  Model page. Querys page k from API with custom filters and
    sorting order. */
import React, { useEffect, useState, useRef } from "react";
import Grid from "@material-ui/core/Grid";
import CallbackPaginationTable, { DropdownMenu } from "./poll-utils";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import { Link } from "react-router-dom";
import { Textfield } from "react-mdl";
import { Menu, MenuItem } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import LinearProgress from "@material-ui/core/LinearProgress";

const model_attributes = [
  { id: "id", label: "ID", no_search: true },
  { id: "party", label: "party", no_search: true },
  { id: "pollster", label: "Pollster" },
  { id: "sponsor", label: "Sponsor" },
  { id: "fte_grade_num", label: "FTE Grade", no_search: true },
  { id: "methodology", label: "Methodology" },
  { id: "created_at", Label: "Time created" }
];

const filter_attr = [
  { id: "none", label: "none" },
  { id: "DEM", label: "DEM" },
  { id: "REP", label: "REP" }
];

const FormatModel = row => {
  const root = /poll/;
  return (
    <TableRow>
      <TableCell component="th" scope="row">
        <Link
          style={{ textDecoration: "none", color: "fff" }}
          to={{
            pathname: root + row.id
          }}
        >
          {row.id}
        </Link>
      </TableCell>
      <TableCell>{row.party}</TableCell>
      <TableCell>{row.pollster}</TableCell>
      <TableCell>{row.sponsor}</TableCell>
      <TableCell>{row.fte_grade}</TableCell>
      <TableCell>{row.methodology}</TableCell>
      <TableCell>{row.created_at}</TableCell>
    </TableRow>
  );
};

/*Search bar for searching by attribute */
const SearchBar = props => {
  const { selected, onSubmit, onMenuSelect } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const textRef = useRef(); //textfield ref for clearing input

  //menu selection handler
  const handleMenuSelect = selected => event => {
    onMenuSelect(event, selected);
    if (textRef.current !== null) {
      textRef.current.inputRef.value = "";
    }
    setAnchorEl(null);
  };
  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }
  function handleClose(event) {
    setAnchorEl(null);
  }
  //Text field and menu button
  //menu is made by mapping MenuItem components to the attribute list
  return (
    <React.Fragment>
      <Textfield //Search field
        placeholder={selected}
        type="text"
        ref={textRef}
        onKeyPress={e => {
          if (e.key === "Enter") onSubmit(e);
        }}
        label="Expandable Input"
        id="polling-filter"
        expandable
        expandableIcon="search"
      />

      <IconButton aria-controls="long-menu" onClick={handleClick}>
        <MoreVertIcon />
      </IconButton>
      <Menu anchorEl={anchorEl} open={open} keepMounted onClose={handleClose}>
        {model_attributes
          .filter(attr => attr.no_search !== true)
          .map(attr => (
            <MenuItem
              selected={attr.id === selected}
              key={attr.id}
              value={attr.id}
              onClick={handleMenuSelect(attr.id)}
            >
              {attr.label}
            </MenuItem>
          ))}
      </Menu>
    </React.Fragment>
  );
};

const Polling = props => {
  const [pollingData, setPolls] = useState([]);
  const [page, setPage] = useState(0);
  const [numPages, setNumPages] = useState(0);
  const [searchBy, setSearchBy] = useState("pollster");
  const [filterBy, setFilterBy] = useState(undefined);
  const [pollingFilter, setPollingFilter] = useState(undefined);
  const [orderBy, setOrderBy] = useState("created_at");
  const [order, setOrder] = useState("desc");
  const rowsPerPage = 10;

  useEffect(() => {
    async function fetchData() {
      var data = {
        order_by: [],
        filters: []
      };
      if (pollingFilter !== undefined) {
        var searchTerms = pollingFilter.split(" ");
        data.filters = data.filters.concat([
          {
            or: searchTerms.map(term => {
              return { name: searchBy, op: "ilike", val: "%25" + term + "%25" };
            })
          }
        ]);
      }
      if (filterBy !== undefined) {
        data.filters = data.filters.concat([
          {
            name: "party",
            op: "eq",
            val: filterBy
          }
        ]);
      }
      if (data.filters === []) delete data.filters;
      data.order_by = [
        {
          field: orderBy,
          direction: order
        }
      ];
      const QUERY = JSON.stringify(data);
      var pg = page + 1;
      const response = await fetch(
        "https://api.2020guide.me/polls?q=" + QUERY + "&page=" + pg
      );
      const json = await response.json();
      setPolls(json.objects);
      setNumPages(json.total_pages);
    }
    fetchData();
  }, [page, pollingFilter, filterBy, order, searchBy, orderBy]);

  /*function callback definitions*/
  function handleRequestSort(event, property) {
    const isDesc = orderBy === property && order === "desc";
    setOrder(isDesc ? "asc" : "desc");
    setOrderBy(property);
    setPage(0);
  }

  function handleChangePage(event, newPage) {
    setPage(newPage);
  }
  function handleSearchSelect(event, id) {
    setSearchBy(id);
    setPollingFilter(undefined);
  }
  function handleFilter(event, id) {
    if (id === "none") {
      setFilterBy(undefined);
    } else {
      setFilterBy(id);
    }
  }

  function handleSearchSubmit(e) {
    setPollingFilter(e.target.value);
    setPage(0);
  }
  //Textbox, dropdown menu, filter menu, and table.
  return (
    <div>
      {pollingData.length === 0 && <LinearProgress />}
      <React.Fragment>
        <Grid
          container
          direction="row"
          alignItems="center"
          justify="space-between"
        >
          <Grid item>
            <SearchBar
              selected={searchBy}
              onMenuSelect={handleSearchSelect}
              onSubmit={handleSearchSubmit}
            />
          </Grid>
          <Grid item>
            <DropdownMenu
              selected={filterBy}
              onSelect={handleFilter}
              attrs={filter_attr}
            >
              {"Filter By: "}
            </DropdownMenu>
          </Grid>
        </Grid>
        <CallbackPaginationTable
          rows={pollingData}
          rowsPerPage={rowsPerPage}
          rowFormat={FormatModel}
          attributes={model_attributes}
          page={page}
          numPages={numPages}
          onChangePage={handleChangePage}
          order={order}
          orderBy={orderBy}
          onRequestSort={handleRequestSort}
        />
      </React.Fragment>

      <p>
        For an explanation of FTE Grade, please refer to
        <a href=" https://projects.fivethirtyeight.com/pollster-ratings">
          https://projects.fivethirtyeight.com/pollster-ratings/
        </a>
      </p>
    </div>
  );
};

export default Polling;
