import React, { useState } from "react";
import PropTypes from "prop-types";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import { Menu, MenuItem } from "@material-ui/core";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import Button from "@material-ui/core/Button";

const defaultStyle = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3)
  },
  table: {
    minWidth: 500
  },
  tableWrapper: {
    overflowX: "auto"
  }
}));

const useStyles1 = makeStyles(theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing(2.5)
  }
}));

//Header for polling tables with sorting callback
//modified from materialui demos for sorting tables
const PollingHeader = props => {
  const { attributes, order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {attributes.map(col => (
          <TableCell key={col.id}>
            <TableSortLabel
              active={orderBy === col.id}
              direction={order}
              onClick={createSortHandler(col.id)}
            >
              {col.label ? col.label : col.id}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

//modified from the table pagination demos provided with material-ui
const PaginationActions = props => {
  const theme = useTheme();
  const classes = useStyles1(theme);

  //maxPages, number of pages, callback
  //had to be called count to work with tablepaginationactions
  const { count, page, onChangePage } = props;

  //Create handlers for arbitrary page num by partial application
  const handlePageButtonClick = page => event => {
    onChangePage(event, page);
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handlePageButtonClick(0)}
        disabled={page === 0}
        aria-label="First Page"
      >
        <FirstPageIcon />
      </IconButton>
      <IconButton
        onClick={handlePageButtonClick(page - 1)}
        disabled={page === 0}
        aria-label="Previous Page"
      >
        <KeyboardArrowLeft />
      </IconButton>
      {page + 1} of {count}
      <IconButton
        onClick={handlePageButtonClick(page + 1)}
        disabled={page >= count - 1}
        aria-label="Next Page"
      >
        <KeyboardArrowRight />
      </IconButton>
      <IconButton
        onClick={handlePageButtonClick(count - 1)}
        disabled={page >= count - 1}
        aria-label="Last Page"
      >
        <LastPageIcon />
      </IconButton>
    </div>
  );
};
PaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired
};

/*Dropdown menu for filtering by attribute*/
const DropdownMenu = props => {
  const { children, selected, onSelect, attrs } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  //Callbacks
  function openMenu(event) {
    setAnchorEl(event.currentTarget);
  }
  function handleClose(event) {
    setAnchorEl(null);
  }

  //partial function application to create event handlers
  //selecting an item also closes the menu
  const menuSelect = attr_id => event => {
    onSelect(event, attr_id);
    setAnchorEl(null);
  };

  //Button displays text and selected field
  //menu will be mounted on the button, stays open til
  //item selected or clicked off
  return (
    <React.Fragment>
      <Button onClick={openMenu}>
        {children}
        {selected}
        <ArrowDropDownIcon />
      </Button>
      <Menu anchorEl={anchorEl} open={open} keepMounted onClose={handleClose}>
        {attrs.map(attr => (
          <MenuItem
            selected={attr.id === selected}
            key={attr.id}
            value={attr.id}
            onClick={menuSelect(attr.id)}
          >
            {attr.label}
          </MenuItem>
        ))}
      </Menu>
    </React.Fragment>
  );
};

//table with pagination callbacks for use on polling model and instance pages
const CallbackPaginationTable = props => {
  const {
    rows,
    rowsPerPage,
    rowFormat,
    attributes,
    page,
    numPages,
    onChangePage,
    order,
    orderBy,
    onRequestSort
  } = props;
  const classes = defaultStyle();
  //page states

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length);

  return (
    <Paper className={classes.root}>
      <div className={classes.tableWrapper}>
        <Table className={classes.table}>
          <PollingHeader
            order={order}
            orderBy={orderBy}
            onRequestSort={onRequestSort}
            attributes={attributes}
          />
          <TableBody>
            {rows.map(rowFormat)}

            {emptyRows > 0 && (
              <TableRow style={{ height: 48 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                align={"right"}
                rowsPerPageOptions={[{ rowsPerPage }]}
                colSpan={attributes.length}
                count={numPages}
                labelDisplayedRows={() => <div />}
                page={page}
                SelectProps={{
                  inputProps: { "aria-label": "Rows per page" },
                  native: true
                }}
                onChangePage={onChangePage}
                ActionsComponent={PaginationActions}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </div>
    </Paper>
  );
};

CallbackPaginationTable.propTypes = {
  rows: PropTypes.array.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  rowFormat: PropTypes.func.isRequired,
  attributes: PropTypes.array.isRequired,
  page: PropTypes.number.isRequired,
  numPages: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  onRequestSort: PropTypes.func.isRequired
};

export default CallbackPaginationTable;
export { PaginationActions, DropdownMenu };
