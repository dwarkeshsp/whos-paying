import React, { useEffect, useState } from "react";

import PollingTable from "./pollingTable.js";
import { Grid, Cell } from "react-mdl";
import { PieChart } from "react-d3-components";
const attr = [{ id: "candidate_name" }, { id: "pct" }];

//basic pie chart for displaying polling pcts
const Chart = props => {
  var data = {
    label: "label",
    //map polling datum into {x, y} format for chart
    values: props.samples.map(obj => {
      return { x: obj.candidate_name, y: obj.pct };
    })
  };

  var sort = null;
  return (
    <PieChart
      data={data}
      width={600}
      height={400}
      margin={{ top: 10, bottom: 10, left: 100, right: 100 }}
      sort={sort}
    />
  );
};

export default function Poll(props) {
  //url parameters
  const {
    match: { params }
  } = props;
  //id and state hook for asynch fetch call
  const poll_id = params.id;
  const [poll, setPoll] = useState({ samples: [] });

  useEffect(() => {
    //gets the actual data
    async function fetchData() {
      const response = await fetch("https://api.2020guide.me/polls/" + poll_id);
      const json = await response.json();
      json.samples = json.samples.map(entry => {
        var tmp = entry;
        tmp["pct"] = Number(entry["pct"]); //convert pcts to floating point
        return tmp;
      });
      setPoll(json);
    }
    fetchData();
  }, [poll_id]);

  //Format the info using the pie chart and a custom table
  return (
    <div>
      <Grid>
        <Cell col={6} shadow={2}>
          <p>Poll: {poll.id} </p>
          <p>Pollster: {poll.pollster}</p>
          <p>Sponsor: {poll.sponsor ? poll.sponsor : "N/A"}</p>
          <PollingTable rows={poll.samples} attributes={attr} />
        </Cell>
        <Cell col={6} shadow={1}>
          <Chart samples={poll.samples} />
        </Cell>
      </Grid>
      {poll.url ? <p>{poll.url}</p> : <p>source url N/A</p>}
    </div>
  );
}
