/*  Table for displaying results of a poll
    Separate from the model table because of
    pagination */

import React, { useState, useEffect } from "react";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import { Link } from "react-router-dom";
import CallbackPaginationTable from "./poll-utils";

//sorting functions provided with material-ui demos
function desc(a, b, orderBy, numeric) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === "desc"
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const FormatSamples = sample => {
  return (
    <TableRow>
      <TableCell component="th" scope="row">
        <Link
          style={{ textDecoration: "none", color: "fff" }}
          to={{
            pathname: /candidate/ + sample.answer
          }}
        >
          {sample.candidate_name}
        </Link>
      </TableCell>
      <TableCell>{sample.pct}</TableCell>
    </TableRow>
  );
};

const PollingTable = props => {
  const { rows, attributes } = props;

  const [orderBy, setOrderBy] = useState("pct");
  const [order, setOrder] = useState("desc");
  const [page, setPage] = useState(0);
  const [displayRows, setRows] = useState([]);
  const rowsPerPage = 10;

  const numPages = Math.ceil(rows.length / rowsPerPage);

  useEffect(() => {
    setRows(
      stableSort(rows, getSorting(order, orderBy)).slice(
        page * rowsPerPage,
        page * rowsPerPage + rowsPerPage
      )
    );
  }, [page, order, orderBy, rows]);

  function handleRequestSort(event, property) {
    const isDesc = orderBy === property && order === "desc";
    setOrder(isDesc ? "asc" : "desc");
    setOrderBy(property);
    setPage(0);
  }

  function handleChangePage(event, newPage) {
    setPage(newPage);
  }

  return (
    <CallbackPaginationTable
      rows={displayRows}
      rowsPerPage={rowsPerPage}
      rowFormat={FormatSamples}
      attributes={attributes}
      page={page}
      numPages={numPages}
      onChangePage={handleChangePage}
      order={order}
      orderBy={orderBy}
      onRequestSort={handleRequestSort}
    />
  );
};

export default PollingTable;
