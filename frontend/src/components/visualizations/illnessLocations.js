import React, { Component } from "react";
import { BarChart } from "react-d3-components";

class IllnessLocations extends Component {
    componentDidMount() {


    }

    render() {
        const illnessNames = ["Acute Bronchitis", "Acute Respiratory Distress Syndrome", "Asbestosis", "Asthma", "Bronchiectasis", "Bronchiolitis", "Byssinosis", "Coccidioidomycosis", "Cystic Fibrosis", "Emphysema", "Hantavirus Pulmonary Syndrome", "Histoplasmosis", "Human Metapneumovirus", "Hypersensitivity Pneumonitis", "Influenza", "Lung Cancer", "Lymphanglioleimyomatosis", "Mesothelioma", "Middle Eastern Respiratory Syndrome", "Nontuberculosis Mycobacteria", "Pneumonia", "Primary Ciliary Dyskinesia", "Respiratory Syncytial Virus", "Sarcoidosis", "Severe Acute Respiratory Syndrome", "Silicosis", "Sleep Apnea", "Sudden Infant Death Syndrome", "Tuberculosis"];

        const locationsPer = [3, 0, 2, 11, 4, 4, 9, 1, 2, 3, 3, 1, 4, 2, 2, 7, 3, 3, 2, 1, 3, 4, 4, 2, 9, 2, 4, 1, 5];

        //data has to be in multi-dimensional format to accomodate different data sets
        //here we just use one
        var data = [{ values: [] }];

        //push the data in the arrays above into the data variable in the correct format
        for (var index = 0; index < illnessNames.length; index ++) {
            data[0]["values"].push({ x: illnessNames[index], y: locationsPer[index] });
        }

        return <BarChart
            data={data}
            width={1000}
            height={500}
            //tickDirection allows for longer names not to overlap
            xAxis = {{label: "Illnesses", tickDirection: "vertical"}}
            yAxis = {{label: "Cities with High Rates of this Illness"}}
            margin={{ top: 10, bottom: 250, left: 50, right: 10 }} />;
    }
}
export default IllnessLocations;