import React, { Component } from "react";
import AgeChart from "./ageChart";
import IllnessLocations from "./illnessLocations";
import PollutantIllnesses from "./pollutantIllnesses";
import CityPlot from "./cityPlot";
import LocationChart from "./locationChart"

class Visualizations extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <center><h3>Additional Visualizations of Our Data</h3></center>
        <center><h5>Candidate Ages</h5></center>
        <center><AgeChart /></center>
        <center><h5>Candidate Birth Places</h5></center>
        <center><LocationChart /></center>
        <center><h3>Visualizations Of Our Developer's Data</h3></center>
        <center><h5>Number of Cities Each Illness is Found in</h5></center>
        <center><IllnessLocations /></center>
        <center><h5>Number of Pollutants Causing Each Illness</h5></center>
        <center><PollutantIllnesses /></center>
        <center><h5>Cities' Air Qualities vs Density</h5></center>
        <center><CityPlot /></center>
      </div>
    );
  }
}

export default Visualizations;