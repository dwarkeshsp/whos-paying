import React, { Component } from "react";
import { BarChart } from "react-d3-components";

class LocationChart extends Component {
  componentDidMount() {

    
  }
  
  render() {
    const locations = ["OH", "NY", "MT", "DC", "WA", "MA", "AS", 
                        "IN", "FL", "India", "NJ", "CA", "TX", "OK",
                    "MN", "PA", "IA"];
    const counts = [1, 8, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 3, 1, 2, 3, 1];
   
    var data = [{values: []}];
    for (var i = 0; i < locations.length; i++) {
        data[0]["values"].push({x: locations[i], y: counts[i]});
    }
   
    return <BarChart
        data={data}
        width={1000}
        height={400}
        xAxis = {{label: "Locations"}}
        yAxis = {{label: "Number of Candidates born in this location"}}
        margin={{top: 10, bottom: 50, left: 50, right: 10}}/>;
  }
}
export default LocationChart;