import React, { Component } from "react";
import { BarChart } from "react-d3-components";

class AgeChart extends Component {
  componentDidMount() {

    
  }
  
  render() {
    const ages = [1, 2, 0, 1, 0, 0, 0, 2, 1, 2, 0, 1, 0, 1, 0, 1, 1, 2, 0, 1, 
        0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 3, 1, 0, 1, 0, 0, 2, 0, 0, 1, 1, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
   
    var data = [{values: []}];
    ages.forEach(function (value, i) {
        if (value !== 0) {
            data[0]["values"].push({x: String(i + 37), y: value});
        }
    });
   
    return <BarChart
        data={data}
        width={1000}
        height={400}
        xAxis = {{label: "Ages"}}
        yAxis = {{label: "Number of Candidates at this Age"}}
        margin={{top: 10, bottom: 50, left: 50, right: 10}}/>;
  }
}
export default AgeChart;