import React, { Component } from "react";
import { ScatterPlot } from "react-d3-components";

class CityPlot extends Component {
    componentDidMount() {


    }

    render() {
        //data obtained from howsmyair.me
        //2 is most dense, 0 is least
        const densities = [2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0];
        const qualities = [27, 40, 32, 47, 35, 39, 20, 37, 38, 45, 34, 43, 45, 10, 38, 27, 40, 45];

        //data has to be in multi-dimensional format to accomodate different data sets
        //here we just use one
        var data = [{ values: [] }];

        //push the data in the arrays above into the data variable in the correct format
        for (var index = 0; index < densities.length; index ++) {
            data[0]["values"].push({ x: (densities[index] - 1), y: qualities[index] });
        }

        return <ScatterPlot
            data={data}
            width={800}
            height={500}
            xAxis = {{label: "------> More Dense"}}
            yAxis = {{label: "Air Quality Index (PM2.5)"}}
            margin={{ top: 10, bottom: 250, left: 50, right: 10 }} />;
    }
}
export default CityPlot;