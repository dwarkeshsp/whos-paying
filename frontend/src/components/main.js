import React from "react";
import { Switch, Route } from "react-router-dom";
import LandingPage from "./landingpage";
import About from "./about";
import Polling from "./polling/polling";
import Poll from "./polling/poll";
import Candidates from "./candidates/candidates";
import CandidateWall from "./candidates/candidateWall";
import Positions from "./positions/positions";
import PositionsWall from "./positions/positionsWall";
import SearchResults from "./search/searchResults";
import Visualizations from "./visualizations/visualizations"

const Main = () => (
  <Switch>
    <Route exact path="/" component={LandingPage} />
    <Route path="/about" component={About} />
    <Route path="/positions" component={Positions} />
    <Route path="/policy/:policy" component={PositionsWall} />
    <Route path="/candidates" component={Candidates} />
    <Route path="/polling" component={Polling} />
    <Route path="/poll/:id" component={Poll} />
    <Route path="/candidate/:name" component={CandidateWall} />
    <Route path="/search" component={SearchResults} />
    <Route path="/visualizations" component={Visualizations} />

  </Switch>
);

export default Main;
