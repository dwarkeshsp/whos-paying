import React, { Component } from "react";
import PositionsTable from "./positionsTable";
import PositionsTableSearch from "./positionsTableSearch";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { lighten, makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import LinearProgress from "@material-ui/core/LinearProgress";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Textfield } from "react-mdl";
import { PaginationActions } from "../polling/poll-utils";

class Positions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      objects: [],
      allObjects: [],
      searchObjects: [],
      searchItem: "",
      filtering: false,
      page: 0,
      count: 2
    };

    this.handleClick = e => {
      e.preventDefault();
    };
  }

  changePage = (e, newPage) => {
    this.setState({ page: newPage });
  };
  async componentDidMount() {
    await fetch("https://api.2020guide.me/issues")
      .then(r => r.json())
      .then(data =>
        this.setState({ objects: data.objects, allObjects: data.objects })
      );
    await fetch("https://api.2020guide.me/issues?page=2")
      .then(r => r.json())
      .then(data => {
        for (const object of data.objects) {
          this.state.allObjects.push(object);
        }
        this.setState({ allObjects: this.state.allObjects });
      });
  }

  componentDidUpdate(prevState) {
    if ((this.state.page !== prevState.page) & !this.state.filtering) {
      fetch("https://api.2020guide.me/issues?page=" + (this.state.page + 1))
        .then(r => r.json())
        .then(data => this.setState({ objects: data.objects }));
    }
  }

  render() {
    return (
      <div>
        {this.state.objects.length === 0 && <LinearProgress />}
        <Textfield
          onChange={e => {
            let objects = this.state.allObjects;
            let searchObjects = [];
            if (e.target.value === "") {
              this.setState({
                searchObjects: searchObjects
              });
              return;
            }
            for (const object of objects) {
              if (
                object["name"]
                  .toLowerCase()
                  .includes(e.target.value.toLowerCase())
              ) {
                for (const position of object["positions"]) {
                  searchObjects.push(position);
                }
              }
              object["positions"].forEach(function(position) {
                for (const key of Object.keys(position)) {
                  if (
                    key.toLowerCase().includes(e.target.value.toLowerCase())
                  ) {
                    searchObjects.push(position);
                  }
                }
                if (
                  position["Candidate"]
                    .toLowerCase()
                    .includes(e.target.value.toLowerCase())
                ) {
                  searchObjects.push(position);
                }
              });
            }
            this.setState({
              searchObjects: searchObjects
            });
            this.setState({
              searchItem: e.target.value
            });
          }}
          label="Expandable Input"
          id="polling-filter"
          expandable
          expandableIcon="search"
        />
        {this.state.searchObjects.length > 0 && (
          <PositionsTableSearch
            item={this.state.searchItem}
            policies={this.state.searchObjects}
          />
        )}
        {this.state.allObjects.map(policy => (
          <Tooltip title="Filter list">
            <IconButton
              aria-label="Filter list"
              onClick={async e => {
                await this.setState({ filtering: true, objects: [policy] });
              }}
            >
              {policy.name}
            </IconButton>
          </Tooltip>
        ))}
        <Tooltip title="Filter list">
          <IconButton
            aria-label="Filter list"
            onClick={async e => {
              await this.setState({ filtering: false });
            }}
          >
            Show All
          </IconButton>
        </Tooltip>
        {this.state.objects.map(function(policy) {
          return (
            <div>
              <ExpansionPanel>
                <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <EnhancedTableToolbar name={policy.name} />
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <PositionsTable
                    name={policy.name}
                    policies={policy.positions}
                  />
                </ExpansionPanelDetails>
              </ExpansionPanel>
            </div>
          );
        })}
        <PaginationActions
          count={this.state.count}
          page={this.state.page}
          onChangePage={this.changePage}
        />
      </div>
    );
  }
}

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1)
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85)
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
  spacer: {
    flex: "1 1 100%"
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: "0 0 auto"
  }
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();

  return (
    <Toolbar className={clsx(classes.root)}>
      <div className={classes.title}>
        <Typography variant="h6" id="tableTitle">
          {props.name}
        </Typography>
      </div>
    </Toolbar>
  );
};

export default Positions;
