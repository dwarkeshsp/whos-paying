import React, { Component } from "react";
import urlTitles from "./urlTitles";
import PositionsWallTable from "./positionsWallTable";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

class PositionsWall extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apiInformation: {
        title: "",
        extract: "",
        image: ""
      },
      candidates: {}
    };
    this.getCandidatesPositions = this.getCandidatesPositions.bind(this);
  }
  componentDidMount() {
    const { name } = this.props.location.state;
    this.setState({ name: name });
    const wikiPageTitle = urlTitles[name];

    fetch("https://api.2020guide.me/issues")
      .then(r => r.json())
      .then(data => {
        data.objects.map(category =>
          this.getCandidatesPositions(category, name)
        );
      });

    //What I need; check apiInformation in state
    fetch("https://api.2020guide.me/positions/" + wikiPageTitle)
      .then(r => r.json())
      .then(data => this.setState({ apiInformation: data }));
  }

  getCandidatesPositions(category, name) {
    for (const candidate in category.positions) {
      if (category.positions[candidate][name] !== undefined) {
        const candidateName = category.positions[candidate]["Candidate"];
        const candidatePosition = category.positions[candidate][name];
        this.setState(prevState => {
          let candidates = Object.assign({}, prevState.candidates);
          candidates[candidateName] = candidatePosition;
          return { candidates };
        });
      }
    }
  }

  render() {
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item md={9} xs={12}>
            <h2>{this.state.name}</h2>
            <img
              src={this.state.apiInformation.image}
              alt="Position"
              class="w-100"
            />
            <h4>{this.state.apiInformation.extract}</h4>
          </Grid>
          <Grid item md={3} xs={12}>
            <Paper>
              <PositionsWallTable candidates={this.state.candidates} />
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default PositionsWall;
