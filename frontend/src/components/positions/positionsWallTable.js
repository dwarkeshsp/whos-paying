import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto"
  },
  table: {
    minWidth: 150
  }
}));

export default function PositionsWallTable(props) {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Candidate</TableCell>
            <TableCell align="right">Positions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {Object.keys(props.candidates).map(candidate => (
            <TableRow>
              <TableCell>
                {
                  <Link
                    style={{ textDecoration: "none", color: "fff" }}
                    to={{
                      pathname:
                        /candidate/ +
                        candidate
                          .split(" ")
                          .slice(-1)
                          .join(" ")
                    }}
                  >
                    {candidate}
                  </Link>
                }
              </TableCell>
              <TableCell align="right">{props.candidates[candidate]}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}
