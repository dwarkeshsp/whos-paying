import React, { Component } from "react";
import { Link } from "react-router-dom";
import PositionsTableSearch from "../positions/positionsTableSearch";

class SearchResults extends Component {
  constructor(props) {
    super(props);

    // get query
    const query = this.props.location.search.replace("?q=", "");

    this.state = {
      query: query,
      candidates: [],
      possibleIssues: [],
      issues: [],
      polls: []
    };
  }

  searchCandidates() {
    var query = {
      filters: [
        {
          or: [{ name: "name", op: "ilike", val: "%" + this.state.query + "%" }]
        }
      ]
    };

    fetch("https://api.2020guide.me/candidates?q=" + JSON.stringify(query))
      .then(response => response.json())
      .then(data => this.setState({ candidates: data.objects }));
  }

  async searchPositions() {
    await fetch("https://api.2020guide.me/issues")
      .then(r => r.json())
      .then(data => {
        console.log(data.objects);
        this.setState({ possibleIssues: data.objects });
      });
    await this.parsePositions();
  }

  async parsePositions() {
    const query = this.props.location.search.replace("?q=", "");
    this.setState({ query: query });
    let searchObjects = [];
    if (query === "") {
      this.setState({ issues: searchObjects });
      return;
    }
    for (const object of this.state.possibleIssues) {
      if (object["name"].toLowerCase().includes(query.toLowerCase())) {
        for (const position of object["positions"]) {
          searchObjects.push(position);
        }
      }
      for (const position of object.positions) {
        for (const key of Object.keys(position)) {
          if (key.toLowerCase().includes(query.toLowerCase())) {
            searchObjects.push(position);
          }
        }
        if (position["Candidate"].toLowerCase().includes(query.toLowerCase())) {
          searchObjects.push(position);
        }
      }
    }
    this.setState({ issues: searchObjects });
  }

  searchPolls() {
    var query = {
      filters: [
        {
          or: [
            {
              name: "pollster",
              op: "ilike",
              val: "%" + this.state.query + "%"
            }
          ]
        }
      ]
    };
    console.log("https://api.2020guide.me/polls?q=" + JSON.stringify(query));
    fetch("https://api.2020guide.me/polls?q=" + JSON.stringify(query))
      .then(response => response.json())
      .then(data => this.setState({ polls: data.objects }));
  }

  componentDidMount() {
    this.searchCandidates();
    this.searchPositions();
    this.searchPolls();
  }

  render() {
    return (
      <div>
        <h1>Candidates</h1>
        {this.state.candidates.map(candidate => {
          return (
            <p>
              <Link to={"/candidate/" + candidate.lastName}>
                {highlightText(candidate.name, this.state.query)}
              </Link>
            </p>
          );
        })}
        <h1>Issues</h1>
        {this.state.issues !== null && this.state.issues.length > 0 && (
          <PositionsTableSearch
            item={this.state.query}
            policies={this.state.issues}
          />
        )}
        <h1>Polls</h1>
        {this.state.polls.map(poll => {
          return (
            <p>
              <Link to={"/poll/" + poll.id}>
                {highlightText(poll.pollster, this.state.query)}
              </Link>
            </p>
          );
        })}
      </div>
    );
  }
}

function highlightText(txt, value) {
  let idx = txt.toLowerCase().indexOf(value.toLowerCase());
  if (idx >= 0)
    var newText = [
      txt.substring(0, idx),
      <mark>{txt.substring(idx, idx + value.length)}</mark>,
      txt.substring(idx + value.length)
    ];
  else newText = txt;
  return newText;
}

export default SearchResults;
