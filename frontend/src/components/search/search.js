import React, { Component } from "react";
import { Textfield } from "react-mdl";
import { Redirect } from "react-router-dom";

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { query: "" };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    this.setState({ query: this.search.inputRef.value });
  }

  render() {
    if (this.state.searching) {
      this.setState({ searching: false });
      if (window.location.href.indexOf("search") >= 0)
        document.location = "/search?q=" + this.state.query;
      return <Redirect to={"/search?q=" + this.state.query} />;
    }
    var searchLabel = "Sidebar Search";
    if (this.props.nav) searchLabel = "Search";
    return (
      <form onSubmit={() => this.setState({ searching: true })}>
        <Textfield
          value={this.state.query}
          ref={input => (this.search = input)}
          onChange={this.handleChange}
          label={searchLabel}
          expandable
          expandableIcon="search"
        />
        <button type="submit" style={{ display: "none" }}>
          Submit
        </button>
      </form>
    );
  }
}

export default SearchBar;
