import React, { Component, useState, useEffect } from "react";
import { Card, CardTitle, CardText, Grid, Cell, Textfield } from "react-mdl";
import { Link } from "react-router-dom";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import LinearProgress from "@material-ui/core/LinearProgress";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { DropdownMenu, PaginationActions } from "../polling/poll-utils";

const filter_attr = [
  { id: "none", label: "None" },
  { id: "R", label: "REP" },
  { id: "D", label: "DEM" }
];

const sort_attr = [
  { id: "none", label: "None" },
  { id: "age", label: "Candidate Age" },
  { id: "lastName", label: "Last Name" },
  { id: "state", label: "Birth Place" }
];
const sort_options = [
  { id: "asc", label: "Ascending" },
  { id: "desc", label: "Descending" }
];

const Candidates = props => {
  const [page, setPage] = useState(0);
  const [objects, setObjects] = useState([]);
  const [numPages, setNumPages] = useState(1);
  const [query, setQuery] = useState(undefined);
  const [filterBy, setFilterBy] = useState("none");
  const [orderBy, setOrderBy] = useState("none");
  const [direction, setDirection] = useState("asc");
  useEffect(() => {
    async function fetchPage() {
      var data = {};
      //if user is searching something
      if (query !== undefined) {
        data.filters = [
          {
            or: [
              { name: "lastName", op: "ilike", val: "%25" + query + "%25" },
              { name: "name", op: "ilike", val: "%25" + query + "%25" }
            ]
          }
        ];
      }
      if (filterBy !== "none") {
        data.filters = data.filters ? data.filters : [];
        data.filters.push({
          name: "party",
          op: "eq",
          val: filterBy
        });
      }
      if (orderBy !== "none") {
        data.order_by = [
          {
            field: orderBy,
            direction: direction
          }
        ];
      }
      const QUERY = JSON.stringify(data);
      let pg = page + 1;
      const response = await fetch(
        "https://api.2020guide.me/candidates?q=" + QUERY + "&page=" + pg
      );
      const json = await response.json();
      setObjects(json.objects);
      setNumPages(json.total_pages);
    }
    fetchPage();
  }, [page, query, filterBy, direction, orderBy]);

  function handleChangePage(event, newPage) {
    setPage(newPage);
  }

  function handleSelectFilter(event, attr_id) {
    setFilterBy(attr_id);
    setPage(0);
  }
  function handleSelectSort(event, attr_id) {
    setOrderBy(attr_id);
  }
  function handleSelectDir(event, attr_id) {
    setDirection(attr_id);
  }
  return (
    <div>
      <div>
        {objects.length === 0 && <LinearProgress />}
        <Textfield
          id="cand_search_field"
          onKeyPress={e => {
            if (e.key === "Enter") {
              setQuery(e.target.value);
              setPage(0);
            }
          }}
          label="Search for candidate"
          floatingLabel
          style={{ marginLeft: 200, width: "200px" }}
        />
      </div>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <DropdownMenu
          selected={filterBy}
          onSelect={handleSelectFilter}
          attrs={filter_attr}
        >
          Filter By:{" "}
        </DropdownMenu>
        <DropdownMenu
          // select attribute to sort by
          selected={sort_attr.find(x => x.id === orderBy).label}
          onSelect={handleSelectSort}
          attrs={sort_attr}
        >
          Sort By:{" "}
        </DropdownMenu>
        {orderBy === "none" ? null : (
          <DropdownMenu
            //select direction to sort by
            selected={direction}
            onSelect={handleSelectDir}
            attrs={sort_options}
          />
        )}
      </div>
      <Grid justify="center" spacing={4}>
        {objects.map(candidate => (
          <Cell col={4}>
            <CandidateCard candidate={candidate} />
          </Cell>
        ))}
      </Grid>
      <Grid container justify="center">
        <Grid item xs={3}>
          <PaginationActions
            page={page}
            count={numPages}
            onChangePage={handleChangePage}
          />
        </Grid>
      </Grid>
    </div>
  );
};

class CandidateCard extends Component {
  render(props) {
    return (
      <div>
        <Card
          shadow={0}
          style={{
            margin: "auto",
            width: "auto",
            "margin-top": "25px"
            // "margin-bottom": "25px"
          }}
        >
          <Link
            id="cardLearnMore"
            to={{
              pathname: /candidate/ + this.props.candidate.lastName
            }}
          >
            <CardTitle
              style={{
                color: "#fff",
                height: "300px",
                background:
                  "url(" + this.props.candidate.image + ") center / cover"
              }}
            >
              {this.props.candidate.name}
            </CardTitle>
          </Link>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              Quick Peek
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div>
                <Grid spacing={4}>
                  <Cell col={6}>
                    <h5>Party: {this.props.candidate.party}</h5>
                  </Cell>
                  <Cell col={6}>
                    <h5>Age: {this.props.candidate.age}</h5>
                  </Cell>
                  <Cell col={6}>
                    <h5>State: {this.props.candidate.state}</h5>
                  </Cell>
                </Grid>
                <CardText>
                  {this.props.candidate.description.substring(0, 1000)}...
                </CardText>
              </div>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Card>
      </div>
    );
  }
}

export default Candidates;
