import React, { useState, useEffect } from "react";
import { LineChart } from "react-d3-components";
import * as d3 from "d3";
import ScatterPlot from "react-d3-components/lib/ScatterPlot";
import { makeStyles } from "@material-ui/core/styles";

const graphStyles = makeStyles(theme => ({
  root: {
    position: "relative",
    height: 425,
    width: 800
  },

  lineGraph: {
    position: "absolute"
  }
}));

export default function PollLineGraph(props) {
  const classes = graphStyles();
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([{ label: "", values: [{ x: 0, y: 0 }] }]);
  const [fit, setFit] = useState([{ label: "", values: [{ x: 0, y: 0 }] }]);
  const { name } = props;
  const [range, setRange] = useState(100);
  const [domain, setDomain] = useState(null);
  useEffect(() => {
    async function fetchPolls() {
      var maxPct = 0;
      let pg = 1;
      var total_pages = 0;
      var points = [];
      const parseDate = d3.timeParse("%Y/%m/%d");
      var weeks = {};
      const weekFormat = d3.timeFormat("%U/%Y");
      const weekParse = d3.timeParse("%U/%Y");
      do {
        const response = await fetch(
          "https://api.2020guide.me/candidates/" + name + "/polls?page=" + pg
        );
        const json = await response.json();
        const results = json.objects;
        total_pages = json.total_pages;

        for (const result of results) {
          let date = parseDate(result.poll.created_at);
          let pct = Number(result.pct);
          if (pct > maxPct) {
            maxPct = pct;
          }
          let week = weekFormat(date);

          if (weeks[week] === undefined) {
            weeks[week] = { sum: 0, count: 0 }; //won't divide by zero bc always incremented at least once
          }
          weeks[week].sum += pct;
          weeks[week].count += 1;

          points.push({
            x: date,
            y: pct
          });
        }
        pg += 1;
      } while (pg <= total_pages);
      points = points.sort((a, b) => {
        return b.x - a.x;
      });

      var fit_values = [];
      for (const week of Object.keys(weeks)) {
        fit_values.push({
          x: weekParse(week),
          y: weeks[week].sum / weeks[week].count
        });
      }
      fit_values = fit_values.sort((a, b) => {
        return b.x - a.x;
      });

      setRange(Math.ceil(maxPct / 10) * 10);
      setDomain(d3.extent(points.map(obj => obj.x)));
      setData([
        {
          label: name,
          values: points
        }
      ]);
      setFit([
        {
          label: "fit line",
          values: fit_values
        }
      ]);
      setLoading(false);
    }
    if (name !== undefined) {
      fetchPolls();
    }
  }, [name]);

  if (loading === true) {
    return <div>Loading...</div>;
  } else {
    if (data.length !== 0) {
      var yScale = d3
        .scaleLinear()
        .domain([0, range]) //axis range (0 to 100 pct)
        .range([340, 10]); //we're mapping onto the pixel size
      var xScale = d3
        .scaleTime()
        .domain(domain)
        .range([0, 690]);
      var color = ["#d62728"];
      return (
        <div className={classes.root}>
          <div className={classes.lineGraph}>
            <ScatterPlot
              yScale={yScale}
              xScale={xScale}
              colorScale={() => color}
              data={data}
              width={750}
              height={400}
              margin={{ top: 10, bottom: 50, left: 50, right: 10 }}
              yAxis={{ label: "pct", innerTickSize: 10 }}
              xAxis={{ tickDirection: "diagonal", ticks: d3.timeMonth }}
            />
          </div>
          <div className={classes.lineGraph}>
            <LineChart
              yScale={yScale}
              xScale={xScale}
              data={fit}
              width={750}
              height={400}
              margin={{ top: 10, bottom: 50, left: 50, right: 10 }}
              yAxis={{
                innerTickSize: 10,
                tickFormat: () => {
                  return "";
                }
              }}
              xAxis={{
                ticks: d3.timeMonth,
                tickFormat: () => {
                  return "";
                }
              }}
            />
          </div>
        </div>
      );
    } else {
      return <div>something broke</div>;
    }
  }
}
