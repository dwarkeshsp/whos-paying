import React, { Component } from "react";
import { Button } from "react-mdl";
export default class CandidateSearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      terms: ""
    };
  }

  updateSearch() {
    let input = document.getElementById("search_recipe").value;
    this.state.searchFunc(input);
  }

  render() {
    return (
      <div>
        <Button id="search_button" onClick={() => this.updateSearch()}>
          Search
        </Button>
      </div>
    );
  }
}
