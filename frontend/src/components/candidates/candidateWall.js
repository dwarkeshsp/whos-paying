import React, { Component, useEffect, useState } from "react";
import { Grid, Cell, ProgressBar } from "react-mdl";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Link, Redirect } from "react-router-dom";
import PollLineGraph from "./pollLineGraph";

const Wall = props => {
  const { name, candidate, policies, error } = props;
  const [expanded, setExpanded] = useState(false);

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  if (error === true) {
    return <Redirect to={"/search?q=" + name} />;
  }
  return (
    <div>
      <Grid>
        <Cell col={4} tablet={12}>
          <div>
            <img src={candidate.image} alt="avatar" style={{ width: "95%" }} />
          </div>
          <h2>{candidate.name}</h2>
          <h4 style={{ color: "grey" }}>{candidate.position}</h4>
          <hr style={{ borderTop: "3px solid #833fb2", width: "50%" }} />
          <p>{candidate.description}</p>
          <hr style={{ borderTop: "3px solid #833fb2", width: "50%" }} />
          <h5>Website</h5>
          <p>
            <a href={candidate.website}>{candidate.website}</a>
          </p>
          <h5>Twitter</h5>
          <p>
            <a href={"https://twitter.com/" + candidate.twitter}>
              {candidate.twitter}
            </a>
          </p>
          <hr style={{ borderTop: "3px solid #833fb2", width: "50%" }} />
        </Cell>
        <Cell className="resume-right-col" col={8} tablet={12}>
          <blockquote>{candidate.quote}</blockquote>
          <hr style={{ borderTop: "3px solid #e22947" }} />
          <h2>Polling History</h2>
          <PollLineGraph name={candidate.lastName} />
          <hr style={{ borderTop: "3px solid #e22947" }} />
          <h2>Policies</h2>
          <div>
            {policies.map((policy, index) => (
              <Policy
                issue={policy}
                i={index}
                handleChange={handleChange}
                expanded={expanded}
              />
            ))}
          </div>
        </Cell>
      </Grid>
    </div>
  );
};
const CandidateWall = props => {
  const {
    match: { params }
  } = props;
  const name = params.name;
  const [candidate, setCandidate] = useState({});
  const [policies, setPolicies] = useState([]);
  const [error, setError] = useState(false);
  useEffect(() => {
    async function fetchCandidate() {
      const response = await fetch(
        "https://api.2020guide.me/candidates/" + name
      );
      const json = await response.json();
      if (Object.entries(json).length === 0) {
        console.log("JSON was undefined");
        setError(true);
      }
      setCandidate(json);
      setPolicies(json.policies);
    }
    fetchCandidate();
  }, [name]);

  return (
    <Wall name={name} candidate={candidate} policies={policies} error={error} />
  );
};

class Policy extends Component {
  render() {
    return (
      <ExpansionPanel
        expanded={this.props.expanded === "panel" + this.props.i}
        onChange={this.props.handleChange("panel" + this.props.i)}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          // aria-controls="panel1a-content"
          // id="panel1a-header"
        >
          <h6 style={{ fontWeight: "bold" }}>{this.props.issue.name}</h6>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography style={{ width: "100%" }}>
            {this.props.issue.issues.map(issue => {
              return (
                <Grid>
                  <Cell col={6} tablet={12}>
                    <Link
                      to={{
                        pathname: /policy/ + issue.policy,
                        state: { name: issue.policy }
                      }}
                    >
                      {issue.policy}
                    </Link>
                  </Cell>
                  <Cell col={6} tablet={12}>
                    <p style={{ float: "right" }}>100%</p>
                    <p>0%</p>
                    <ProgressBar progress={issue.position} />
                  </Cell>
                </Grid>
              );
            })}
          </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

export default CandidateWall;
