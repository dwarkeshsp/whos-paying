import React, { Component } from "react";
import { Card, CardTitle, CardText, CardActions, CardMenu } from "react-mdl";
import { Link } from "react-router-dom";
import { Grid, Cell } from "react-mdl";

class CandidateCard extends Component {
  render(props) {
    return (
      <div>
        <Card
          shadow={0}
          style={{
            margin: "auto",
            "margin-top": "25px",
            "margin-bottom": "25px"
          }}
        >
          <CardTitle
            style={{
              color: "#fff",
              height: "200px",
              background:
                "url(" + this.props.candidate.image + ") center / cover"
            }}
          >
            {this.props.candidate.name}
          </CardTitle>
          <CardText>
            ({this.props.candidate.party}) {this.props.candidate.description}
          </CardText>
          <CardText>
            ({this.props.candidate.party}) {this.props.candidate.description}
          </CardText>
          <CardActions border>
            <Link
              style={{ textDecoration: "none", color: "fff" }}
              to={{
                pathname: /candidate/ + this.props.candidate.lastName,
                state: { candidate: this.props.candidate }
              }}
            >
              Learn More
            </Link>
          </CardActions>
          <CardMenu style={{ color: "#fff" }} />
        </Card>
      </div>
    );
  }
}

class Candidates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      candidates: [
        {
          name: "Donald Trump",
          lastName: "trump",
          party: "R",
          age: 73,
          position: "President",
          experience: [
            {
              jobName: "President",
              startYear: 2017,
              endYear: "Now",
              jobDescription:
                "POTUS is the head of state and head of government of the United States of America. The president directs the executive branch of the federal government and is the commander-in-chief of the United States Armed Forces."
            },
            {
              jobName: "Businessman",
              startYear: 1971,
              endYear: 2017,
              jobDescription:
                "Trump company built or renovated skyscrapers, hotels, casinos, and golf courses. Trump later started various side ventures, mostly by licensing his name."
            }
          ],
          policies: [
            {
              policy: "Single Payer Health Care",
              position: 0
            },
            {
              policy: "Free Public College",
              position: 0
            },
            {
              policy: "Reparations for Slavery",
              position: 0
            }
          ],
          image:
            "https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fmedia1.s-nbcnews.com%2Fj%2Fnewscms%2F2016_51%2F1843361%2F161223-donald-trump-mn-0830_d4802f202768fd044f1b4919e3c8879f.nbcnews-fp-1200-800.jpg&f=1",
          website: "https://www.donaldjtrump.com",
          twitter: "https://twitter.com/realdonaldtrump",
          description:
            "45th and current president of the United States. Before entering politics, he was a businessman and television personality.",
          quote:
            "We will make America strong again. We will make America proud again. We will make America safe again. And we will Make America Great Again!"
        },
        {
          name: "Joe Biden",
          lastName: "biden",
          party: "D",
          age: 76,
          position: "Former Vice President and Senator",
          experience: [
            {
              jobName: "Vice President",
              startYear: 2009,
              endYear: 2017,
              jobDescription:
                "Second-highest officer in the executive branch of the U.S. federal government, after the president of the United States, and ranks first in the presidential line of succession. The vice president is also an officer in the legislative branch, as President of the Senate."
            },
            {
              jobName: "Senator from Delaware",
              startYear: 1973,
              endYear: 2009,
              jobDescription:
                "Represented the interests of Delaware in the United States Senate. He chaired the Foreign Relations Committee and the Judiciary Committee."
            }
          ],
          policies: [
            {
              policy: "Single Payer Health Care",
              position: 0
            },
            {
              policy: "Free Public College",
              position: 50
            },
            {
              policy: "Reparations for Slavery",
              position: 0
            }
          ],
          image:
            "https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fd.ibtimes.co.uk%2Fen%2Ffull%2F1537098%2Fdnc-2016.jpg&f=1",
          website: "https://joebiden.com/",
          twitter: "https://twitter.com/joebiden",
          description:
            "Served as the 47th vice president of the United States from 2009 to 2017. Represented Delaware in the U.S. Senate from 1973 to 2009.",
          quote:
            "It’s time for respected leadership on the world stage—and dignified leadership at home. It’s time for equal opportunity, equal rights, and equal justice. It’s time for an economy that rewards those who actually do the work. It’s time for a president who will stand up for all of us."
        },
        {
          name: "Elizabeth Warren",
          lastName: "warren",
          party: "D",
          age: 70,
          position: "Senator from Massachusetts",
          experience: [
            {
              jobName: "Senator from Massachusetts",
              startYear: 2013,
              endYear: "Now",
              jobDescription:
                "Represented the interests of Massachusetts in the United States Senate. She chaired the Congressional Oversight Panel and helped create the Consumer Financial Protection Bureau."
            }
          ],
          policies: [
            {
              policy: "Single Payer Health Care",
              position: 100
            },
            {
              policy: "Free Public College",
              position: 100
            },
            {
              policy: "Reparations for Slavery",
              position: 100
            }
          ],
          image:
            "https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.thenation.com%2Fwp-content%2Fuploads%2F2018%2F08%2Felizabeth-warren-woburn-ap-img.jpg&f=1",
          website: "https://elizabethwarren.com/",
          twitter: "https://twitter.com/SenWarren",
          description:
            "American politician and former academic currently serving as the senior United States Senator from Massachusetts since 2013. Warren was formerly a law school professor specializing in bankruptcy law. A progressive, she has focused on consumer protection, economic opportunity, and the social safety net while in the Senate.",
          quote:
            "Americans are fighters. We're tough, resourceful and creative, and if we have the chance to fight on a level playing field, where everyone pays a fair share and everyone has a real shot, then no one - no one can stop us."
        },
        {
          name: "Donald Trump",
          lastName: "trump",
          party: "R",
          age: 73,
          position: "President",
          experience: [
            {
              jobName: "President",
              startYear: 2017,
              endYear: "Now",
              jobDescription:
                "POTUS is the head of state and head of government of the United States of America. The president directs the executive branch of the federal government and is the commander-in-chief of the United States Armed Forces."
            },
            {
              jobName: "Businessman",
              startYear: 1971,
              endYear: 2017,
              jobDescription:
                "Trump company built or renovated skyscrapers, hotels, casinos, and golf courses. Trump later started various side ventures, mostly by licensing his name."
            }
          ],
          policies: [
            {
              policy: "Single Payer Health Care",
              position: 0
            },
            {
              policy: "Free Public College",
              position: 0
            },
            {
              policy: "Reparations for Slavery",
              position: 0
            }
          ],
          image:
            "https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fmedia1.s-nbcnews.com%2Fj%2Fnewscms%2F2016_51%2F1843361%2F161223-donald-trump-mn-0830_d4802f202768fd044f1b4919e3c8879f.nbcnews-fp-1200-800.jpg&f=1",
          website: "https://www.donaldjtrump.com",
          twitter: "https://twitter.com/realdonaldtrump",
          description:
            "45th and current president of the United States. Before entering politics, he was a businessman and television personality.",
          quote:
            "We will make America strong again. We will make America proud again. We will make America safe again. And we will Make America Great Again!"
        },
        {
          name: "Joe Biden",
          lastName: "biden",
          party: "D",
          age: 76,
          position: "Former Vice President and Senator",
          experience: [
            {
              jobName: "Vice President",
              startYear: 2009,
              endYear: 2017,
              jobDescription:
                "Second-highest officer in the executive branch of the U.S. federal government, after the president of the United States, and ranks first in the presidential line of succession. The vice president is also an officer in the legislative branch, as President of the Senate."
            },
            {
              jobName: "Senator from Delaware",
              startYear: 1973,
              endYear: 2009,
              jobDescription:
                "Represented the interests of Delaware in the United States Senate. He chaired the Foreign Relations Committee and the Judiciary Committee."
            }
          ],
          policies: [
            {
              policy: "Single Payer Health Care",
              position: 0
            },
            {
              policy: "Free Public College",
              position: 50
            },
            {
              policy: "Reparations for Slavery",
              position: 0
            }
          ],
          image:
            "https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fd.ibtimes.co.uk%2Fen%2Ffull%2F1537098%2Fdnc-2016.jpg&f=1",
          website: "https://joebiden.com/",
          twitter: "https://twitter.com/joebiden",
          description:
            "Served as the 47th vice president of the United States from 2009 to 2017. Represented Delaware in the U.S. Senate from 1973 to 2009.",
          quote:
            "It’s time for respected leadership on the world stage—and dignified leadership at home. It’s time for equal opportunity, equal rights, and equal justice. It’s time for an economy that rewards those who actually do the work. It’s time for a president who will stand up for all of us."
        },
        {
          name: "Elizabeth Warren",
          lastName: "warren",
          party: "D",
          age: 70,
          position: "Senator from Massachusetts",
          experience: [
            {
              jobName: "Senator from Massachusetts",
              startYear: 2013,
              endYear: "Now",
              jobDescription:
                "Represented the interests of Massachusetts in the United States Senate. She chaired the Congressional Oversight Panel and helped create the Consumer Financial Protection Bureau."
            }
          ],
          policies: [
            {
              policy: "Single Payer Health Care",
              position: 100
            },
            {
              policy: "Free Public College",
              position: 100
            },
            {
              policy: "Reparations for Slavery",
              position: 100
            }
          ],
          image:
            "https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.thenation.com%2Fwp-content%2Fuploads%2F2018%2F08%2Felizabeth-warren-woburn-ap-img.jpg&f=1",
          website: "https://elizabethwarren.com/",
          twitter: "https://twitter.com/SenWarren",
          description:
            "American politician and former academic currently serving as the senior United States Senator from Massachusetts since 2013. Warren was formerly a law school professor specializing in bankruptcy law. A progressive, she has focused on consumer protection, economic opportunity, and the social safety net while in the Senate.",
          quote:
            "Americans are fighters. We're tough, resourceful and creative, and if we have the chance to fight on a level playing field, where everyone pays a fair share and everyone has a real shot, then no one - no one can stop us."
        }
      ]
    };
  }
  render() {
    return (
      <div>
        <Grid spacing={4}>
          {this.state.candidates.map(candidate => (
            <Cell col={3}>
              <CandidateCard candidate={candidate} />
            </Cell>
          ))}
        </Grid>
      </div>
    );
  }
}

export default Candidates;
