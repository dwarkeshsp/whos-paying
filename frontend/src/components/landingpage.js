import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Jumbotron, Button } from "reactstrap";
import { UncontrolledCarousel } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";

const items = [
  {
    src: require("../pictures/white.jpg"),
    altText: "title and white house"
  },
  {
    src: require("../pictures/donald.jpg"),
    altText: "donald trump"
  },
  {
    src: require("../pictures/biden.jpg"),
    altText: "biden"
  },
  {
    src: require("../pictures/kamala.jpg"),
    altText: "kamala"
  },
  {
    src: require("../pictures/sanders.jpg"),
    altText: "sanders"
  },
  {
    src: require("../pictures/warren.jpg"),
    altText: "warren"
  }
];

class Landing extends Component {
  render() {
    return (
      <div>
        <UncontrolledCarousel items={items} />

        <Jumbotron>
          <h1 class="text-center">Your one stop shop for 2020</h1>
          <section className="text-center">
            <p>
              2020 guide has all a voter could want to get informed about the
              2020 election. Learn about the candidates, their positions, and
              the polls.
            </p>
          </section>
        </Jumbotron>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Link to="/candidates">
            <Button type="button" size="lg">
              Candidates
            </Button>
          </Link>
          <Link to="/positions">
            <Button type="button" size="lg">
              Positions
            </Button>
          </Link>
          <Link to="/polling">
            <Button type="button" size="lg">
              Polling
            </Button>
          </Link>
        </div>
      </div>
    );
  }
}

export default Landing;
