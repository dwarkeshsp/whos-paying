import React from "react";
import "./App.css";
import { Layout, Header, Navigation, Drawer, Content } from "react-mdl";
import Main from "./components/main";
import { Link } from "react-router-dom";
import SearchBar from "./components/search/search";

function App() {
  return (
    <div className="demo-big-content">
      <Layout>
        <Header
          className="header-color"
          title={
            <Link style={{ textDecoration: "none", color: "white" }} to="/">
              2020 Guide
            </Link>
          }
          scroll
        >
          <Navigation>
            <Link id="navcand" to="/candidates">
              Candidates
            </Link>
            <Link id="navpos" to="/positions">
              Positions
            </Link>
            <Link id="navpolls" to="/polling">
              Polling
            </Link>
            <Link id="navvis" to="/visualizations">
              Visualizations
            </Link>
            <Link id="navabout" to="/about">
              About
            </Link>
          </Navigation>
          <SearchBar nav={true} />
        </Header>
        <Drawer title="2020 Guide">
          <Navigation>
            <Link to="/candidates">Candidates</Link>
            <Link to="/positions">Positions</Link>
            <Link to="/polling">Polling</Link>
            <Link to="/visualizations">Visualizations</Link>
            <Link to="/about">About</Link>
          </Navigation>
          <SearchBar nav={false} />
        </Drawer>
        <Content>
          <div className="page-content" />
          <Main />
        </Content>
      </Layout>
    </div>
  );
}

export default App;
