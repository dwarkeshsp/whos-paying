import React from "react";
import Enzyme, { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import App from "../App";
import Landing from "../components/landingpage";
import Candidates from "../components/candidates/candidates";
import About from "../components/about";
import Polling from "../components/polling/polling";
import Positions from "../components/positions/positions";
import PollsAvg from "../components/candidates/candidateWall";

Enzyme.configure({ adapter: new Adapter() });

// test if failing works

//test if components are rendering
describe("First React component test with Enzyme", () => {
  it("renders without crashing", () => {
    shallow(<App />);
  });
});

describe("landing page test", () => {
  it("should render landing page", () => {
    const temp = shallow(<Landing />);
    expect(temp.exists()).toBe(true);
  });
});

describe("landing page snapshot", () => {
  it("should match", () => {
    const temp = shallow(<Landing />);
    expect(temp).toMatchSnapshot();
  });
});

describe("candidates page test", () => {
  it("should render candidates page", () => {
    const temp = shallow(<Candidates />);
    expect(temp.exists()).toBe(true);
  });
});

describe("candidates page snapshot", () => {
  it("should match", () => {
    const temp = shallow(<Candidates />);
    expect(temp).toMatchSnapshot();
  });
});

describe("polling test", () => {
  it("should render polling", () => {
    const temp = shallow(<Polling />);
    expect(temp.exists()).toBe(true);
  });
});

describe("polling page snapshot", () => {
  it("should match", () => {
    const temp = shallow(<Polling />);
    expect(temp).toMatchSnapshot();
  });
});

describe("positions test", () => {
  it("should render positions", () => {
    const temp = shallow(<Positions />);
    expect(temp.exists()).toBe(true);
  });
});

describe("positions page snapshot", () => {
  it("should match", () => {
    const temp = shallow(<Positions />);
    expect(temp).toMatchSnapshot();
  });
});

describe("about test", () => {
  it("should render about", () => {
    const temp = shallow(<About />);
    expect(temp.exists()).toBe(true);
  });
});

describe("about test snapshot", () => {
  it("should match", () => {
    const temp = shallow(<About />);
    expect(temp).toMatchSnapshot();
  });
});

// describe("candidates page test 2", () => {
//   const container = shallow(<PollsAvg party={"D"} candidateName={"Bennet"} />);
//   expect(container.prop("party"));
//   console.log(container.prop("party"));
// });
