from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait

import unittest
import time


class TestingClass(unittest.TestCase):
    def setUp(self):  # unittest construct
        testOptions = webdriver.ChromeOptions()
        testOptions.add_argument("--no-sandbox")
        testOptions.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(options=testOptions)
        self.url = "http://localhost:3000/"
        self.driver.implicitly_wait(5)

    def tearDown(self):
        self.driver.quit()

    def test_1_title(self):
        self.driver.get(self.url)
        self.assertEqual("2020 Guide", self.driver.title)

    def test_2_landing_button_cand(self):
        try:
            self.driver.get(self.url)
            landing = self.driver.find_element_by_link_text("Candidates")
            landing.click()
            self.assertEqual(self.driver.current_url, self.url + "candidates")
        except NoSuchElementException as err:
            self.fail(err.msg)

    def test_3_landing_button_pos(self):
        try:
            self.driver.get(self.url)
            landing = self.driver.find_element_by_link_text("Positions")
            landing.click()
            self.assertEqual(self.driver.current_url, self.url + "positions")
        except NoSuchElementException as err:
            self.fail(err.msg)

    def test_4_landing_button_poll(self):
        try:
            self.driver.get(self.url)
            landing = self.driver.find_element_by_link_text("Polling")
            landing.click()
            self.assertEqual(self.driver.current_url, self.url + "polling")
        except NoSuchElementException as err:
            self.fail(err.msg)

    def test_6_nav_about(self):
        try:
            self.driver.get(self.url)
            landing = self.driver.find_element_by_id("navabout")
            landing.click()
            self.assertEqual(self.driver.current_url, self.url + "about")
        except NoSuchElementException as err:
            self.fail(err.msg)

    def test_7_nav_cand(self):
        try:
            self.driver.get(self.url)
            landing = self.driver.find_element_by_id("navcand")
            landing.click()
            self.assertEqual(self.driver.current_url, self.url + "candidates")
        except NoSuchElementException as err:
            self.fail(err.msg)

    def test_8_nav_pos(self):
        try:
            self.driver.get(self.url)
            landing = self.driver.find_element_by_id("navpos")
            landing.click()
            self.assertEqual(self.driver.current_url, self.url + "positions")
        except NoSuchElementException as err:
            self.fail(err.msg)

    def test_9_nav_poll(self):
        try:
            self.driver.get(self.url)
            landing = self.driver.find_element_by_id("navpolls")
            landing.click()
            self.assertEqual(self.driver.current_url, self.url + "polling")
        except NoSuchElementException as err:
            self.fail(err.msg)

    # search for bennet in the candidates search bar, then click on the first result
    # should take you to bennet's page
    def test_10_cand_trump_search(self):
        try:
            self.driver.get(self.url)
            el = WebDriverWait(self.driver, 3).until(
                ec.presence_of_element_located((By.ID, "textfield-Search"))
            )
            el.send_keys("trump")
            el.send_keys(Keys.RETURN)
            target = self.driver.find_element_by_link_text("Donald J. Trump")
            target.click()
            self.assertEqual(self.driver.current_url, self.url + "candidate/Trump")
        except NoSuchElementException as err:
            self.fail(err.msg)

    # always intercepted somehow
    # def test_11_polling_filter(self):
    #     try:
    #         # click filter button
    #         self.driver.get(self.url + "polling")
    #         el = self.driver.find_element_by_xpath(
    #             '//*[@id="root"]/div/div/div/div[3]/div[2]/div[1]/div[2]/button'
    #         )
    #         el.click()

    #         # click dem sort
    #         preTarget = self.driver.find_element_by_xpath(
    #             "/html/body/div[3]/div[3]/ul/li[2]"
    #         )
    #         preTarget.click()

    #         # click poll ID
    #         target = WebDriverWait(self.driver, 3).until(
    #             ec.presence_of_element_located(
    #                 (
    #                     By.XPATH,
    #                     '//*[@id="root"]/div/div/div/div[3]/div[2]/div[2]/div/table/tbody/tr[1]/th/a',
    #                 )
    #             )
    #         )
    #         target.click()
    #         self.assertEqual(self.driver.current_url, self.url + "polling/99088")
    #     except NoSuchElementException as err:
    #         self.fail(err.msg)

    # also intercepted...
    # def test_12_positions_sort(self):
    #     try:
    #         self.driver.get(self.url + "positions")
    #         el = WebDriverWait(self.driver, 3).until(
    #             ec.presence_of_element_located((By.CLASS_NAME, "MuiSvgIcon-root"))
    #         )
    #         el.click()
    #         e2 = WebDriverWait(self.driver, 3).until(
    #             ec.presence_of_element_located(
    #                 (
    #                     By.XPATH,
    #                     '//*[@id="panel1a-content"]/div/div/div/div[1]/table/thead/tr/th[3]/span/svg',
    #                 )
    #             )
    #         )
    #         e2.click()
    #         self.assertEqual(self.driver.current_url, self.url + "candidate/Trump")
    #     except NoSuchElementException as err:
    #         self.fail(err.msg)


# Main Method
if __name__ == "__main__":
    unittest.main()
