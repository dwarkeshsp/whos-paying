import os

from flask import Flask
from flask_cors import CORS
from flask_restless import APIManager
from flask_sqlalchemy import SQLAlchemy

import cache
from utils import slugify


# ---------------------
# Initialization/Set-up
# ---------------------


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DEBUG = False

application = app = Flask(__name__)
if DEBUG:
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
        BASE_DIR, "db.sqlite3"
    )
else:
    app.config[
        "SQLALCHEMY_DATABASE_URI"
    ] = "postgresql+psycopg2://guideme:testpassword@guideme.cuq2uctdrv5p.us-east-2.rds.amazonaws.com:5432/guideme"
db = SQLAlchemy(app)
CORS(app)


# ------
# Models
# ------


class Candidates(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    status = db.Column(db.String)
    lastName = db.Column(db.String)
    state = db.Column(db.String)
    party = db.Column(db.String)
    age = db.Column(db.Integer)
    image = db.Column(db.String)
    website = db.Column(db.String)
    twitter = db.Column(db.String)
    description = db.Column(db.Text)
    quote = db.Column(db.Text)

    # Candidate's positions
    positions = db.relationship(
        "CandidatePositions", backref="candidate", lazy="dynamic"
    )
    polls = db.relationship("PollSamples", backref="candidate", lazy=True)

    @classmethod
    def query(cls):  # Custom query method to get 2020 presidential candidates.
        original_query = db.session.query(cls)
        condition = Candidates.status != "Inactive"
        return original_query.filter(condition)

    @classmethod
    def policies(cls, result=None, **kw):
        # Documentation says to put kw for future purposes
        if "id" in result:  # Indicates we want a single resource
            id = result["id"]
            d = {}
            candidate = db.session.query(cls).get(id)
            for position in candidate.positions:
                issue_name = position.policy.issue.name
                if issue_name not in d:
                    d[issue_name] = []

                # Convert Candidate's position to a number
                if position.position == "Yes" or position.position == "$15":
                    pos = 100
                elif position.position == "Partial":
                    pos = 50
                else:
                    pos = 0

                d[issue_name].append({
                    "policy": position.policy.name,
                    "position": pos
                })
            result["policies"] = [{"name": key, "issues": d[key]} for key in d]


class Issues(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

    candidates_positions = db.relationship(
        "Positions", backref=db.backref("issue", lazy=True)
    )

    def positions(self):
        data, age = cache.load()

        if self.name in data and data[self.name] and (age < 86400):
            # TODO: Cache based on age?
            #       or delete cache when Candidates, Positions, or Issues gets
            #       modified.
            l = data[self.name]
        else:
            # Get the positions
            l = []
            for candidate in Candidates.query():
                d = {}
                positions = candidate.positions.join(Positions).filter(
                    Positions.issue_id == self.id
                )
                if positions:
                    d["Candidate"] = candidate.name
                    d["Status"] = candidate.status

                    for position in positions:
                        d[position.policy.name] = position.position
                    l.append(d)

            # Update the cache
            data[self.name] = l
            cache.update(data)
        return l


class Positions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    wikiPageTitle = db.Column(db.String)
    slug = db.Column(db.String)
    extract = db.Column(db.Text)
    image = db.Column(db.String)

    issue_id = db.Column(db.Integer, db.ForeignKey("issues.id"), nullable=False)
    # Candidates' stance on this position
    stances = db.relationship("CandidatePositions", backref="policy", lazy=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.slug = slugify(self.name)  # Fill in slug field automatically

    def candidates(self):
        l = []
        for candidate in self.stances:
            l.append(
                {"candidate": candidate.candidate.name, "position": candidate.position}
            )
        return l


class CandidatePositions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    position_id = db.Column(db.Integer, db.ForeignKey("positions.id"), nullable=False)
    candidate_id = db.Column(db.Integer, db.ForeignKey("candidates.id"), nullable=False)
    position = db.Column(db.String)


class Polls(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pollster = db.Column(db.String)
    sponsor = db.Column(db.String)
    fte_grade = db.Column(db.String)
    methodology = db.Column(db.String)
    created_at = db.Column(db.String)
    party = db.Column(db.String)
    sample_size = db.Column(db.Integer)
    fte_grade_num = db.Column(db.Integer)
    unix_time = db.Column(db.Integer)

    poll_samples = db.relationship("PollSamples", backref="poll", lazy=True)

    @classmethod
    def samples(cls, result=None, **kw):
        if "id" in result:
            l = []
            for sample in cls.query.get(result["id"]).poll_samples:
                l.append(
                    {
                        "party": sample.party,
                        "answer": sample.answer,
                        "pct": sample.pct,
                        "candidate_name": sample.candidate.name,
                    }
                )
            result["samples"] = l


class PollSamples(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    party = db.Column(db.String)
    answer = db.Column(db.String)
    pct = db.Column(db.String)
    candidate_id = db.Column(db.Integer, db.ForeignKey("candidates.id"), nullable=False)
    poll_id = db.Column(db.Integer, db.ForeignKey("polls.id"), nullable=False)


# -----
# Views
# -----


@app.route("/")
def index():
    return """
    <h3> <b>Endpoints</b> </h3>
      <p>
        /candidates<br>
        /issues<br>
        /positions<br>
        /polls<br>
        /candidates/name<br>
        /issues/name<br>
        /positions/wikiPageTitle<br>
        /polls/id<br>
        <br>
        <a href="https://documenter.getpostman.com/view/7912158/S1a61kjs?version=latest">Postman Documentation</a>
      </p>
    """


# This is how our API views are automatically created for us.
apimanager = APIManager(app, flask_sqlalchemy_db=db)
candidates = apimanager.create_api(
    Candidates,
    url_prefix="",
    primary_key="lastName",
    exclude_columns=["positions", "polls"],
    max_results_per_page=9,
    postprocessors={"GET_SINGLE": [Candidates.policies]},
)
issues = apimanager.create_api(
    Issues,
    url_prefix="",
    primary_key="name",
    include_methods=["positions"],
    exclude_columns=["candidates_positions"],
)
positions = apimanager.create_api(
    Positions,
    url_prefix="",
    primary_key="wikiPageTitle",
    include_methods=["candidates"],
    exclude_columns=["stances"],
)
polls = apimanager.create_api(
    Polls, url_prefix="", postprocessors={"GET_SINGLE": [Polls.samples]}
)

if __name__ == "__main__":
    app.run(debug=DEBUG)

# Here's an example of a query http://127.0.0.1:5000/candidates?q={"filters":[{"name":"image","op":"==","val":"Test"}]}
