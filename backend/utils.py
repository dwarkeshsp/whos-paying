import unicodedata
import re


def slugify(name):
    slug = unicodedata.normalize("NFKD", name)
    slug = slug.encode("ascii", "ignore").lower().decode()
    slug = re.sub(r"[^a-z0-9]+", "-", slug).strip("-")
    slug = re.sub(r"[-]+", "-", slug)
    return slug
