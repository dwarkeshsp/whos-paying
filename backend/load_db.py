import json
import csv
from application import Candidates, db, Issues, Polls
from scrapepositions import scrape_candidate_positions


def load_candidates():
    with open("candidates-final.json", "r") as f:
        data = json.load(f)
    for i, candidate in enumerate(data):
        c = Candidates(
            id=i,
            name=candidate.get("name").title(),
            lastName=candidate.get("lastName").title(),
            party=candidate.get("party"),
            # state=candidate.get("state"],
            image=candidate.get("image"),
            website=candidate.get("website"),
            twitter=candidate.get("twitter"),
            description=candidate.get("description"),
        )
        db.session.add(c)
        db.session.commit()


def load_issues():
    data = scrape_candidate_positions()

    for i, item in enumerate(data):
        issue = Issues(id=i, **item)
        db.session.add(issue)
        db.session.commit()


def load_polls():
    def get_obj_from_id(id):
        for item in data:
            if item["id"] == id:
                return item
        return None

    data = []
    with open("president_primary_polls.csv", mode="r") as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            item = get_obj_from_id(row["question_id"])
            if not item:
                item = {
                    "id": row["question_id"],
                    "pollster": row["pollster"],
                    "sponsor": row["sponsors"],
                    "fte_grade": row["fte_grade"],
                    "methodology": row["methodology"],
                    "created_at": row["created_at"],
                    "party": row["party"],
                    "sample_size": row["sample_size"],
                    "samples": list(),
                }
                data.append(item)
            item["samples"].append(
                {
                    "candidate_name": row["candidate_name"],
                    "answer": row["answer"],
                    "pct": row["pct"],
                    "party": row["party"],
                }
            )
    for item in data:
        poll = Polls(**item)
        db.session.add(poll)
        db.session.commit()


if __name__ == "__main__":
    # load_candidates()
    # load_issues()
    load_polls()
