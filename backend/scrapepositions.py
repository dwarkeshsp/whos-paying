import json
import requests
from wikitables import import_tables


def scrape_candidate_positions():
    """Scrapes for Candidate's Positions"""

    def get_candidate(l, name):
        for c in l:
            if c["name"] == name:
                return c

        return {"name": name}

    title = "Political_positions_of_the_2020_Democratic_Party_presidential_primary_candidates"

    # Get Table of Contents
    r = requests.get(
        "http://en.wikipedia.org/w/api.php?action=parse&format=json&prop=sections&page=%s&redirects"
        % title
    ).json()
    toc = []
    for section in r["parse"]["sections"]:
        if section["toclevel"] == 2:
            toc += [section["line"]]

    # Get Tables
    tables = import_tables(title)

    l = []  # List of Issues
    for i, table in enumerate(tables):
        table.name = toc[i]
        for row in table.rows:
            for key in row:
                # Get Candidate's Position on Topic
                if "{{No}}" in row[key].raw:
                    row[key].value = "No"
                elif "{{Yes}}" in row[key].raw:
                    row[key].value = "Yes"
                elif "{{Partial}}" in row[key].raw:
                    row[key].value = "Partial"
                elif "{{Open}}" in row[key].raw:
                    row[key].value = "Open"
                elif "{{Unknown}}" in row[key].raw:
                    row[key].value = "Unknown"
        l += [{"name": table.name, "positions": json.loads(table.json())}]

    return l


if __name__ == "__main__":
    scrape_candidate_positions()
