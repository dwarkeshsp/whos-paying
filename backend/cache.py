import json
import os
import time

fname = "issues-positions-cache.json"

if not os.path.isfile(fname):  # Check if the file exists
    # If it doesn't, create the file.
    with open(fname, "a") as f:
        f.write("{}")


def load():
    # Open file and load the data.
    with open(fname, "r") as f:
        cache = json.load(f)
    return cache, os.path.getmtime(fname) - time.time()


def update(cache):
    with open(fname, "w") as f:
        json.dump(cache, f)


def remove(key=None, wipe=False):
    """"""
    if wipe:
        with open(fname, "w") as f:
            json.dump({}, f)
    elif key:
        cache = load()[0]
        if key in cache:
            cache.pop(key)
            update(cache)
