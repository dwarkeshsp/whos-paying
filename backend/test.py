import json
import os
import unittest
import io

from application import app, db
from scrapepositions import scrape_candidate_positions
from scrapecandidates import scrape_fec
from utils import slugify
import cache


class CacheTests(unittest.TestCase):
    def test_cache_load(self):
        data, age = cache.load()
        self.assertIsInstance(data, dict)

    def test_cache_wipe(self):
        cache.remove(wipe=True)
        data, age = cache.load()
        self.assertEquals(data, {})

    def test_cache_update(self):
        cache.update({"test": "Hi!"})
        data, age = cache.load()
        self.assertTrue("test" in data)

    def test_cache_remove(self):
        cache.remove(key="test")
        data, age = cache.load()
        self.assertFalse("test" in data)


class ScraperTests(unittest.TestCase):
    def test_scrape_positions(self):
        with io.open("test_scrape_positions.txt", "r", encoding="utf-8") as f:
            try:
                json.loads(f.read())
            except ValueError:
                self.fail("test_scrape_positions raised ValueError")

    def test_scrape_candidates(self):
        with io.open("test_scrape_candidates.txt", "r", encoding="utf-8") as f:
            try:
                json.loads(f.read())
            except ValueError:
                self.fail("test_scrape_candidates raised ValueError")


class SlugifyTests(unittest.TestCase):
    def test_slugify_1(self):
        val = slugify("Test Text")
        self.assertEquals(val, "test-text")


# TODO: Test more models!


if __name__ == "__main__":
    unittest.main()
