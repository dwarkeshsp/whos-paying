from datetime import date
import json
import requests


def scrape_fec():
    # https://api.open.fec.gov/developers/
    params = {
        "election_year": 2020,
        "page": 1,
        "api_key": "KwBdUyXOzet3pFfguNIHXAHzN0OeBMsbqsFLdtvx",
        "is_active_candidate": True,
        "candidate_status": "C",
        "office": "P",
    }

    url = "https://api.open.fec.gov/v1/candidates/"

    res = requests.get(url, params=params).json()
    pagination = res["pagination"]
    l = list()

    while params["page"] <= pagination["pages"]:
        for candidate in res["results"]:
            name = candidate["name"].split(", ")
            l.append(
                {
                    "name": candidate["name"],
                    "lastName": name[0],
                    "fec_id": candidate["candidate_id"],
                    "party": candidate["party"][0],
                    "state": candidate["state"],
                }
            )

        params["page"] += 1
        res = requests.get(url, params=params).json()

    return l


def scrape_propublica(chamber, l):
    def calculateAge(birthDate):
        birthDate = date(*map(int, birthDate.split("-")))
        today = date.today()
        age = (
            today.year
            - birthDate.year
            - ((today.month, today.day) < (birthDate.month, birthDate.day))
        )

        return age

    url = "https://api.propublica.org/congress/v1/115/{chamber}/members.json"

    params = {"X-API-Key": "3ppQzjgDtCX0U5z3aVHvf8LUcnZWaov6Qr87iyI4"}
    res = requests.get(url=url, headers=params)
    # print(res.json())
    # with open("test1.txt", "w") as f:
    #     f.write(json.dumps(res.json(), indent=4))
    res = res.json()
    l = json.load(open("candidates-next.json", "r"))
    result = list()
    for m in res["results"][0]["members"]:
        for c in l:
            if c["lastName"] == m["last_name"].upper():
                d = c
                d[
                    "image"
                ] = "https://theunitedstates.io/images/congress/original/{m['id']}.jpg"
                d["website"] = m["url"]
                d["age"] = calculateAge(m["date_of_birth"])
                d["state"] = m["state"]
                d["twitter"] = m["twitter_account"]
                c = d

        # c = Candidate()

    # for c in l:
    #     add = True
    #     for cand in result:
    #         if c["lastName"] == cand["lastName"]:
    #             add = False
    #             break
    #     if add:
    #         result.append(c)

    return l


def scrape_wikipedia(l):
    for c in l:
        url = "https://en.wikipedia.org/w/api.php"
        name = c["name"].split(" ")[1] + " " + c["lastName"]
        params = {
            "action": "query",
            "list": "search",
            "srsearch": name,
            # "srwhat": "title",
            "format": "json",
        }
        r = requests.get(url, params=params).json()
        for s in r["query"]["search"]:
            # print(name.lower())
            # print(s["title"].lower())
            if (
                name.lower() == s["title"].lower()
                or "politician" in s["title"].lower()
                or "Beto" in s["title"]
            ):
                params = {
                    "action": "query",
                    # "list": "search",
                    "pageids": s["pageid"],
                    # "srwhat": "title",
                    "prop": "extracts",
                    "explaintext": True,
                    "exintro": True,
                    "redirects": 1,
                    "format": "json",
                }
                r = requests.get(url, params=params).json()
                # print(r["query"]["pages"][str(s["pageid"])]["extract"])
                c["description"] = r["query"]["pages"][str(s["pageid"])]["extract"]
                break
        # print(r["query"]["search"][0]["title"])

        # params = {
        #     "action": "parse",
        #     "page": c["lastName"] + " " + c["name"].split(" ")[1],
        #     "format": "json"
        # }
        # r = requests.get(url, params=params).json()
        # print(r)
        # print(r["parse"]["title"])

    # with open("candidates-final.json", "w") as f:
    #     json.dump(l, f, indent=4)


def main():
    l = scrape_fec()


if __name__ == "__main__":
    print(scrape_fec())
