import json
import re
import requests
from application import db, Polls
import time
import datetime

pg = 1
resp = requests.get("http://api.2020guide.me/polls?page=" + str(pg))
while resp.json()["objects"] != []:
    for poll in resp.json()["objects"]:
        thisPoll = Polls.query.filter_by(id=poll["id"]).first()
        if thisPoll.unix_time is None:
            """o = thisPoll.created_at
            p = re.split('[/ ]', o)
            n = '20' + p[2] + '/' + p[0] + '/' + p[1]
            """
            n = thisPoll.created_at
            thisPoll.unix_time = int(
                time.mktime(datetime.datetime.strptime(n, "%Y/%m/%d").timetuple())
            )
            # thisPoll.created_at = n
            db.session.commit()

    pg += 1
    resp = requests.get("http://api.2020guide.me/polls?page=" + str(pg))
